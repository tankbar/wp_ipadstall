<?php
/*
Plugin Name: 	    Tankbar Harden WordPress Security
Version: 		    1.0.0
Description: 	    Makes WordPress more secure.
Author:             Tankbar AB
Author URI:         https://www.tankbar.com
Plugin URI:         https://www.tankbar.com
Requires PHP:       7.4
*/

class Tankbar_Harden_WordPress_Security {
	/**
	 * Allowed time between login attempts.
	 * 10 minutes by default.
	 *
	 * @var int|float $time_between_login_attempts
	 */
	private int $time_between_login_attempts = 60 * 10;
	/**
	 * The time a user is being locked out from his or her account.
	 * 60 minutes by default.
	 *
	 * @var int|float $time_locked_out
	 */
	private int $time_locked_out = 60 * 1;

	public function __construct() {
		add_filter( 'rest_endpoints', [ $this, 'filter_rest_endpoints' ] );

		add_filter( 'wp_authenticate_user', [ $this, 'filter_deny_locked_out_user' ], 200, 1 );
		add_action( 'wp_login_failed', [ $this, 'action_check_user_bruteforce_attempts' ], 200, 2 );

		add_action( 'wp_login_failed', [ $this, 'action_clear_incorrect_password_error_message' ], 9999, 2 );
	}

	public function filter_rest_endpoints( $endpoints ) {
		$to_remove = [ 'users' ];

		foreach ( $to_remove as $val ) {
			if ( isset( $endpoints[ '/wp/v2/' . $val ] ) ) {
				$this->harden_endpoint( $endpoints[ '/wp/v2/' . $val ] );
			}

			if ( isset( $endpoints[ '/wp/v2/' . $val . '/(?P<id>[\d]+)' ] ) ) {
				$this->harden_endpoint( $endpoints[ '/wp/v2/' . $val . '/(?P<id>[\d]+)' ] );
			}
		}

		return $endpoints;
	}

	private function harden_endpoint( &$endpoint ) {
		foreach ( $endpoint as &$method ) {
			if ( ! isset( $method['methods'] ) ) {
				continue;
			}

			if ( strpos( $method['methods'], WP_REST_Server::READABLE ) !== false ) {
				$method['permission_callback'] = [ $this, 'authenticated_permission_callback' ];
			}
		}
	}

	public function authenticated_permission_callback() {
		return is_user_logged_in();
	}

	public function filter_deny_locked_out_user( $user ) {
		if ( empty( $user ) || is_wp_error( $user ) ) {
			return $user;
		}

		$user_locked_until = absint( get_user_meta( $user->ID, '_login_locked_until', true ) );

		if ( ! empty( $user_locked_until ) && $user_locked_until - time() > 0 ) {
			$user = new WP_Error(
				'bruteforce_security',
				__( 'Du har försökt logga in för många gånger under en tidsperiod. Var vänlig försök igen senare.', 'tankbar' )
			);
		}

		if ( ! empty( $user_locked_until ) && ! is_wp_error( $user ) ) {
			delete_user_meta( $user->ID, '_login_locked_until' );
			delete_user_meta( $user->ID, '_num_login_attempts' );
			delete_user_meta( $user->ID, '_last_failed_login_attempt' );
		}

		return $user;
	}

	/**
	 * @param          $username
	 * @param WP_Error $error
	 *
	 * @return void
	 */
	public function action_check_user_bruteforce_attempts( $username, $error ) {
		if ( ! in_array( 'incorrect_password', $error->get_error_codes(), true ) ) {
			return;
		}

		$user                    = get_user_by( 'login', $username );
		$user_num_login_attempts = absint( get_user_meta( $user->ID, '_num_login_attempts', true ) );
		$user_last_login_attempt = absint( get_user_meta( $user->ID, '_last_failed_login_attempt', true ) );

		if (
			empty( $user_last_login_attempt ) ||
			time() - $user_last_login_attempt > $this->time_between_login_attempts
		) {
			$user_num_login_attempts = 0;
		}

		update_user_meta( $user->ID, '_last_failed_login_attempt', time() );

		if ( ++ $user_num_login_attempts > 2 ) {
			update_user_meta( $user->ID, '_login_locked_until', time() + $this->time_locked_out );

			foreach ( $error->get_error_codes() as $error_code ) {
				$error->remove( $error_code );
			}

			$error->add(
				'bruteforce_security',
				__( 'Du har försökt logga in för många gånger under en tidsperiod. Var vänlig försök igen senare.', 'tankbar' )
			);
		}

		update_user_meta( $user->ID, '_num_login_attempts', $user_num_login_attempts );
	}

	/**
	 * @param          $username
	 * @param WP_Error $error
	 *
	 * @return void
	 */
	public function action_clear_incorrect_password_error_message( $username, $error ) {
		if ( ! in_array( 'incorrect_password', $error->get_error_codes(), true ) ) {
			return;
		}

		$error->remove( 'incorrect_password' );
		$error->add(
			'failed_authentication',
			__( 'Inloggningen misslyckades. Kontrollera ditt användarnamn och lösenord.', 'tankbar' )
		);
	}
}

new Tankbar_Harden_WordPress_Security();
