<!-- QUOTES BLOCK -->

<?php if( get_field('show_quote') ): ?>
<div class="quote">
  <blockquote class="quote__text">"<?php the_field('show_quote_text'); ?>”</blockquote>
  <?php
	$quote__image = get_field('show_quote_img');
  if( $quote__image ): ?>
    <img src="<?php echo $quote__image['url']; ?>"  class="quote__image">
  <?php endif ?>
  <div class="quote__name"><?php the_field('show_quote_name'); ?></div>
  <div class="quote__title"><?php the_field('show_quote_title'); ?></div>
</div>
<?php endif; ?>
