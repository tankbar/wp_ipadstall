<?php

$image = get_field('promotion__bg-image');

if( !empty($image) ):
	 	$bakimg = 'style="background-image:url('.$image['url'].')"';
 endif; ?>

<div class="bgimg-text-text">
	<div class="bgimg-text-text__inner bgimg-text-text__inner--col1" <?php echo $bakimg ?>>
	<h6 class="block-title"><?php the_field('promotion__pre-title--left'); ?></h6>
	<h4 class="h3 color--pink"><?php the_field('promotion__title--left'); ?></h4>
	<p><?php the_field('promotion__preamble--left'); ?></p>
	</div>

	<div class="bgimg-text-text__inner bgimg-text-text__inner--col2 bgcolor--green">
	<h6 class="block-title"><?php the_field('promotion__pre-title--right'); ?></h6>
	<h4 class="h3 color--pink"><?php the_field('promotion__title--right'); ?></h4>
	<p><?php the_field('promotion__preamble--right'); ?></p>

		<?php
		$link = get_field('promotion__button--right');
		if( $link ): ?>
			<a href="<?php echo $link['url']; ?>" class="cta__button cta__button--white"><?php echo $link['title']; ?></a>
		<?php endif; ?>

	</div>
</div>
