<div class="webbshop">
	<div class="flexible-content__therow webbshop__intro">
		<h6 class="block-title"><?php the_field('webbshop__pre-titel'); ?></h5>
		<h3 class="h2 color--blue"><?php the_field('webbshop__titel'); ?></h3>

		<p class="preamble"><?php the_field('webbshop__preamble'); ?></p>
	</div>


	<?php if( have_rows('webbshop__product') ): ?>

		<div class="webbshop__row">

		<?php while( have_rows('webbshop__product') ): the_row();

			// vars
			$product_img = get_sub_field('webbshop__product__img');
			$product_link = get_sub_field('webbshop__product__link');
			$product_title = get_sub_field('webbshop__product__title');
			$product_category = get_sub_field('webbshop__product__category');
			$product_preamble = get_sub_field('webbshop__product__preamble');
			$product_price = get_sub_field('webbshop__product__price');
			$product_new = get_sub_field('webbshop__product__new');

			if( $product_new ):
				$new_poduct = 'new-product';
			endif;

			?>

			<div class="webbshop__row__product  <?php echo $new_poduct ?>">
				<div class="webbshop__row__product__inner">

					<img class="webbshop__row__product__inner__prod_image new_product" src="<?php echo $product_img['url']; ?>" />

					<h5><?php echo $product_title; ?></h5>
					<div class="webbshop__row__product__inner--prod_cat"><?php echo $product_title; ?></div>
					<div class="webbshop__row__product__inner--prod_desc"><?php echo $product_preamble; ?></div>

					<?php if( $product_link ): ?>
					<div onclick="window.location.href='<?php echo 	$product_link['url'] ?>'" class="cta__button cta__button--shop"><?php echo $product_price; ?> kr</div>
					<?php endif; ?>

				</div>
			</div>

			<?php
			$new_poduct = '';
			?>
		<?php endwhile; ?>
		</div>
	<?php endif; ?>


	<?php	$webbshop_link = get_field('webbshop__lank_till_shop');
	if( $webbshop_link ): ?>

		<div class="cta__blockend">
			<a  href="<?php echo $webbshop_link['url']; ?>" target="<?php echo $webbshop_link['target']; ?>"><?php echo $webbshop_link['title']; ?></a>
		</div>

	<?php endif; ?>

</div>
