<?php
/**
 * The template for displaying the footer
 *
 *
 *
 * @package Tankbar
 * @subpackage Awesome
 * @since Tankbar Awesome 3.0
 */
?>
<?php the_content(); ?>
