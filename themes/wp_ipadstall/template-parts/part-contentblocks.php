


<?php
// check if the repeater field has rows of data
if( have_rows('content_block') ):
 	// loop through the rows of data
    while ( have_rows('content_block') ) : the_row();

			// display a sub field value
			$theposition = get_sub_field('content_block__layout');
			$thetitle = get_sub_field('content_block__title');
			$thetext = get_sub_field('content_block__text');
			?>
			<div class="content__inner content__block content__block--text-<?php echo $theposition ?>">


					<?php

					$images = get_sub_field('content_block__img_gallery');
					$size = 'large'; // (thumbnail, medium, large, full or custom size)

					if( $images ): ?>
					<div class="content__block__img<?php echo $theposition ?>">
					    <ul>
					        <?php foreach( $images as $image ): ?>
					            <li>
					            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
					            </li>
					        <?php endforeach; ?>
					    </ul>
					</div>
					<?php endif; ?>


				<div class=" content__block__text<?php echo $theposition ?>">
					<h3 class="color--blue"><?php echo $thetitle ?></h3>
						<?php echo $thetext ?>
				</div>
					<div class="clearfix"></div>
			</div>
			<?php
			$theposition = "";
			$thetitle = "";
			$thetext = "";
    endwhile;
else :
    // no rows found
endif;

?>
