<span class="single__post-date"><?php echo get_the_date('Y-m-d', $post->ID); ?> </span>
<h1><?php the_title(); ?></h1>
<div class="single__excerpt">
    <?php the_content(); ?>
</div>
