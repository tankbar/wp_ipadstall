
<?php
$rub = get_field('page_header__title');
$text = get_field('page_header__preamble');
?>

	<!-- CONTENT BLOCK INTRO 2 -->
	<div class="content__inner content__block content__block--intro">
		<div class=" content__block__inner maxwidth">
			<h1 class="color--blue"><?php echo $rub ?></h1>
			<div class="preamble">
				<?php echo $text ?>
			</div>
		</div>
	</div>
