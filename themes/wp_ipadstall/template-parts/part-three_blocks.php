                                                                                                                                                                                                                     <div class="three-blocks">
	<div class="three-blocks__block three-blocks__block__one">
		<h3><?php the_field('three_blocks__block_1_title'); ?></h3>
		<p class="preamble"><?php the_field('three_blocks__block_1_preamble'); ?></p>
		<a href="<?php the_field('three_blocks__block_1_button-link'); ?>" class="cta__button cta__button--blue"><?php the_field('three_blocks__block_1_button-text'); ?></a>
	</div>

	<div class="three-blocks__block three-blocks__block__two">
		<h3><?php the_field('three_blocks__block_2_title'); ?></h3>
		<p class="preamble"><?php the_field('three_blocks__block_2_preamble'); ?></p>
		<a href="<?php the_field('three_blocks__block_1_button-link'); ?>" class="cta__button cta__button--blue"><?php the_field('three_blocks__block_2_button-text'); ?></a>
	</div>

	<div class="three-blocks__block three-blocks__block__three">
		<h3><?php the_field('three_blocks__block_3_title'); ?></h3>
		<p class="preamble"><?php the_field('three_blocks__block_2_preamble'); ?></p>
		<a href="<?php the_field('three_blocks__block_3_button-link'); ?>"class="cta__button cta__button--blue"><?php the_field('three_blocks__block_3_button-text'); ?></a>
	</div>
</div>
