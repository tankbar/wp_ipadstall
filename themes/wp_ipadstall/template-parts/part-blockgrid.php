


<?php

$bloggrid = get_field('valj_bloggrid');

if( $bloggrid ): ?>

    <?php foreach( $bloggrid as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($bloggrid); ?>

					<?php
					// vars
					$blockgrid_1_top = get_field('blockgrid__1__top_left_-_big');
          $blockgrid_1_bl = get_field('blockgrid__1__bottom_left_-_small');
          $blockgrid_1_br = get_field('blockgrid__1__bottom_right_-_small');

          $blockgrid_2_tl = get_field('blockgrid__2__top_left_-_small');
          $blockgrid_2_tr = get_field('blockgrid__2__top_right_-_small');
          $blockgrid_2_bottom = get_field('blockgrid__2__bottom_right_-_big');



          ?>
						<?php
  						if( $blockgrid_1_top ):
  							while( have_rows('blockgrid__1__top_left_-_big') ): the_row();

                $blockgrid_1_1_show_bak_img = get_sub_field('blockgrid__show-background-img');
                $blockgrid_1_1_pre_title = get_sub_field('blockgrid__pre-titel');
  							$blockgrid_1_1_title = get_sub_field('blockgrid__title');
                $blockgrid_1_1_preamble = get_sub_field('blockgrid__preamble');
                $blockgrid_1_1_link = get_sub_field('blockgrid__link');


                endwhile;
              endif;

              if( $blockgrid_1_bl ):
                while( have_rows('blockgrid__1__bottom_left_-_small') ): the_row();

                $blockgrid_1_2_show_bak_img = get_sub_field('blockgrid__show-background-img');
                $blockgrid_1_2_pre_title = get_sub_field('blockgrid__pre-titel');
                $blockgrid_1_2_title = get_sub_field('blockgrid__title');
                $blockgrid_1_2_preamble = get_sub_field('blockgrid__preamble');
                $blockgrid_1_2_link = get_sub_field('blockgrid__link');

                endwhile;
              endif;

              if( $blockgrid_1_br ):
                while( have_rows('blockgrid__1__bottom_right_-_small') ): the_row();

                $blockgrid_1_3_show_bak_img = get_sub_field('blockgrid__show-background-img');
                $blockgrid_1_3_pre_title = get_sub_field('blockgrid__pre-titel');
                $blockgrid_1_3_title = get_sub_field('blockgrid__title');
                $blockgrid_1_3_preamble = get_sub_field('blockgrid__preamble');
                $blockgrid_1_3_link = get_sub_field('blockgrid__link');

                endwhile;
              endif;



              if( $blockgrid_2_tl ):
  							while( have_rows('blockgrid__2__top_left_-_small') ): the_row();

                $blockgrid_2_1_show_bak_img = get_sub_field('blockgrid__show-background-img');
                $blockgrid_2_1_pre_title = get_sub_field('blockgrid__pre-titel');
  							$blockgrid_2_1_title = get_sub_field('blockgrid__title');
                $blockgrid_2_1_preamble = get_sub_field('blockgrid__preamble');
                $blockgrid_2_1_link = get_sub_field('blockgrid__link');

                endwhile;
              endif;

              if( $blockgrid_2_tr ):
                while( have_rows('blockgrid__2__top_right_-_small') ): the_row();

                $blockgrid_2_2_show_bak_img = get_sub_field('blockgrid__show-background-img');
                $blockgrid_2_2_pre_title = get_sub_field('blockgrid__pre-titel');
                $blockgrid_2_2_title = get_sub_field('blockgrid__title');
                $blockgrid_2_2_preamble = get_sub_field('blockgrid__preamble');
                $blockgrid_2_2_link = get_sub_field('blockgrid__link');

                endwhile;
              endif;

              if( $blockgrid_2_bottom ):
                while( have_rows('blockgrid__2__bottom_right_-_big') ): the_row();

                $blockgrid_2_3_show_bak_img = get_sub_field('blockgrid__show-background-img');
                $blockgrid_2_3_pre_title = get_sub_field('blockgrid__pre-titel');
                $blockgrid_2_3_title = get_sub_field('blockgrid__title');
                $blockgrid_2_3_preamble = get_sub_field('blockgrid__preamble');
                $blockgrid_2_3_link = get_sub_field('blockgrid__link');

                endwhile;
              endif;


              ?>

              <script>
              function myFunction() {
                  document.getElementById(".myDiv").style.flexGrow = "5";
              }
              </script>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>


	<div class="blockgrid">
		<div class="blockgrid__left-col">
			<div class="blockgrid__lg_block bgcolor--light-pink <?php echo $blockgrid_1_1_show_bak_img ?>" onclick="window.location.href='<?php echo $blockgrid_1_1_link['url'] ?>'">
				<div class="blockgrid__lg_block__inner">
					<h6 class="block-title"><?php echo $blockgrid_1_1_pre_title ?></h5>

					<h3 class="h2 color--blue"><?php echo $blockgrid_1_1_title ?></h3>
					<div class="color--blue upper"><?php echo $blockgrid_1_1_preamble ?></div>

				</div>
			</div>
			<div class="blockgrid__sm_block bgcolor--blue  <?php echo $blockgrid_1_2_show_bak_img ?>" onclick="window.location.href='<?php echo $blockgrid_1_2_link['url'] ?>'">
				<div class="blockgrid__lg_block__inner">
					<h6 class="block-title color--white"><?php echo $blockgrid_1_2_pre_title ?></h5>
						<h4 class="h3 color--pink"><?php echo $blockgrid_1_2_title ?></h3>
				</div>
			</div>
			<div class="blockgrid__sm_block bgcolor--white" <?php echo $blockgrid_1_3_show_bak_img ?> onclick="window.location.href='<?php echo $blockgrid_1_3_link['url'] ?>'">
				<div class="blockgrid__lg_block__inner">
					<h6 class="block-title center-text"><?php echo $blockgrid_1_3_pre_title ?></h5>
					<h4 class="h3 color--pink center-text"><?php echo $blockgrid_1_3_title ?></h3>
				</div>
			</div>

		</div>
		<div class="blockgrid__right-col">
			<div class="blockgrid__sm_block bgcolor--green <?php echo $blockgrid_2_1_show_bak_img ?>" onclick="window.location.href='<?php echo $blockgrid_2_1_link['url'] ?>'">
				<div class="blockgrid__lg_block__inner">
					<h6 class="block-title color--white"><?php echo $blockgrid_2_1_pre_title ?></h5>
					<h4 class="h3 color--pink"><?php echo $blockgrid_2_1_title ?></h3>
				</div>
			</div>
			<div class="blockgrid__sm_block bgcolor--light-blue <?php echo $blockgrid_2_2_show_bak_img ?>" onclick="window.location.href='<?php echo $blockgrid_2_2_link['url'] ?>'">
				<div class="blockgrid__lg_block__inner">
					<h6 class="block-title"><?php echo $blockgrid_2_2_pre_title ?></h5>
					<h4 class="h3 color--blue"><?php echo $blockgrid_2_2_title ?></h3>
				</div>
			</div>
			<div class="blockgrid__lg_block bgcolor--gray <?php echo $blockgrid_2_3_show_bak_img ?>" onclick="window.location.href='<?php echo $blockgrid_2_3_link['url'] ?>'">
				<div class="blockgrid__lg_block__inner">
					<h6 class="block-title color--blue"><?php echo $blockgrid_2_3_pre_title ?></h5>
					<h3 class="h2 color--pink"><?php echo $blockgrid_2_3_title ?></h3>
					<div class="color--pink upper"><?php echo $blockgrid_2_3_preamble ?></div>
				</div>
			</div>
		</div>
	</div>
