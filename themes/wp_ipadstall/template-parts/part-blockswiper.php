<div class="bigblockswiper">
	<div class="swiper-container swiper-bigblock">
		<div class="swiper-wrapper">
			<div class="swiper-slide bgcolor--blue">
				<h6 class="block-title">Case</h5>
				<h4 class="h1 color--pink">Scholler Allibert</h4>
				<div class="case-cat">Transport & Logistik</div>
			</div>
			<div class="swiper-slide bgcolor--light-pink">
				<h6 class="block-title">Case</h5>
				<h4 class="h1 color--blue">Apollo Travel Group</h4>
				<div class="case-cat">ERP-Konsulter & Integration</div>
			</div>
			<div class="swiper-slide bgcolor--blue">
				<h6 class="block-title">Case</h5>
				<h4 class="h1 color--pink">CWT Meetings & Events</h4>
				<div class="case-cat">Transport & Logistik</div>
			</div>
		</div>
		<!-- Add Pagination -->
		<div class="swiper-pagination"></div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.js"></script>
	<!-- Initialize Swiper -->
	<script>
		var swiper = new Swiper('.swiper-bigblock', {
			pagination: '.swiper-pagination',
			direction: 'vertical',
			effect: 'fade',
			mousewheelReleaseOnEdges: true,
			slidesPerView: 1,
			paginationClickable: true,
			spaceBetween: 30,
			mousewheelControl: true,
			parallax: true,
			speed: 600,
		});
	</script>
</div>
