<?php

// check if the flexible content field has rows of data
if( have_rows('blocks') ):

     // loop through the rows of data
    while ( have_rows('blocks') ) : the_row();


		if( get_row_layout() == 'block_100_img_txt_img' ):

			// vars
			$field = get_sub_field_object('block_100__img_txt_img__bgcolor');
			$value = $field['value'];
			?>

			<!-- GRID BLOCK FULL WIDTH W/ CTA -->
			<div class="content__grid">
				<div class="content__grid__full image-text-image <?php echo $value; ?>" style="width: 100%;">


					<?php
					$image = get_sub_field('block_100__img_txt_img__img_left');
					if( !empty($image) ):?>
						<img src="<?php	echo $image['url']; ?>" class="offgrid">
					<?php endif; ?>

					<div class="content__grid__inner">
						<h6 class="block-title"><?php the_sub_field('block_100__img_txt_img__pre-title'); ?></h6>
						<h3 class="h2 color--blue"><?php the_sub_field('block_100__img_txt_img__title'); ?></h3>
						<div class="preamble"><?php the_sub_field('block_100__img_txt_img__text'); ?></div>

							<?php
							$link = get_sub_field('block_100__img_txt_img__link-button');
							if( $link ): ?>
								<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="cta__button cta__button--left"><?php echo $link['title']; ?></a>
							<?php endif; ?>
					</div>
					<?php
					$image2 = get_sub_field('block_100__img_txt_img__img_right');
					if( !empty($image2) ):?>
						<img src="<?php	echo $image2['url']; ?>" class="rightimg">
					<?php endif; ?>

				</div>
			</div>


        <?php elseif( get_row_layout() == 'block_100' ):

					// vars
					$field = get_sub_field_object('block_100__bgcolor');
					$value = $field['value'];
					?>

					<!-- GRID BLOCK FULL WIDTH W/ CTA -->
					<div class="content__grid">
						<div class="content__grid__full <?php echo $value; ?>">
							<div class="content__grid__inner text-center">
								<h6 class="block-title"><?php the_sub_field('block_100__pre-title'); ?></h6>
								<h3 class="h2 color--blue"><?php the_sub_field('block_100__title'); ?></h3>
								<div class="preamble"><?php the_sub_field('block_100__text'); ?></div>

									<?php
									$link = get_sub_field('block_100__link-button');
									if( $link ): ?>
										<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="cta__button cta__button--center"><?php echo $link['title']; ?></a>
									<?php endif; ?>
							</div>
						</div>
					</div>

					<?php
        elseif( get_row_layout() == 'block_70-30' ):

					// vars
					$field = get_sub_field_object('block_70-30__70__bgcolor');
					$value = $field['value'];

					$field2 = get_sub_field_object('block_70-30__30__bgcolor');
					$value2 = $field2['value'];

					?>

					<?php
					$image = get_sub_field('block_70-30__70__bgimage');

					if( !empty($image) ):?>
						<style>
							.content__grid__two-thirds.twothirds.part-background:after
							{
								background-image: url('<?php echo $image; ?>')
							}
						</style>
					<?php endif; ?>

					<?php

					$image2 = get_sub_field('block_70-30__30__bgimage');

					if( !empty($image2) ):?>
						<style>
							.content__grid__one-thirds.twothirds.part-background:after
							{
								background-image: url('<?php 	echo $image2; ?>')
							}
						</style>
					<?php endif; ?>


					<!-- GRID BLOCK 2/3 - 1/3 -->
					<div class="content__grid">
						<div class="content__grid__two-thirds twothirds <?php echo $value; ?> part-background">
							<div class="content__grid__inner">
								<h6 class="block-title"><?php the_sub_field('block_70-30__70__pre-title'); ?></h6>
								<h3 class="h2 color--blue"><?php the_sub_field('block_70-30__70__title'); ?></h3>
								<div class="preamble"><?php the_sub_field('block_70-30__70__text'); ?></div>
							</div>
						</div>
						<div class="content__grid__one-thirds twothirds <?php echo $value2; ?> part-background">
							<div class="content__grid__inner">
								<h6 class="block-title"><?php the_sub_field('block_70-30__30__pre-title'); ?></h6>
								<h3 class="h2 color--pink"><?php the_sub_field('block_70-30__30__title'); ?></h3>
								<div class="preamble"><?php the_sub_field('block_70-30__30__text'); ?></div>
							</div>
						</div>
					</div>

					<?php

					elseif( get_row_layout() == 'block_30-70' ):

						// vars
						$field = get_sub_field_object('block_30-70__30__bgcolor');
						$value = $field['value'];

						$field2 = get_sub_field_object('block_30-70__70__bgcolor');
						$value2 = $field2['value'];

						?>


						<?php
						$image3 = get_sub_field('block_30-70__30__bgimage');

						if( !empty($image3) ):?>
							<style>
								.content__grid__one-thirds.onethirds.part-background:after
								{
									background-image: url('<?php echo $image3; ?>')
								}
							</style>
						<?php endif; ?>
						<?php

						$image4 = get_sub_field('block_30-70__70__bgimage');

						if( !empty($image4) ):?>
							<style>
								.content__grid__two-thirds.onethirds.part-background:after
								{
									background-image: url('<?php	echo $image4; ?>')
								}
							</style>
						<?php endif; ?>


						<!-- GRID BLOCK 1/3 - 2/3 -->
						<div class="content__grid">
							<div class="content__grid__one-thirds onethirds <?php echo $value; ?> part-background">
								<div class="content__grid__inner">
									<h6 class="block-title"><?php the_sub_field('block_30-70__30__pre-title'); ?></h6>
									<h3 class="h2 color--blue"><?php the_sub_field('block_30-70__30__title'); ?></h3>
									<div class="preamble"><?php the_sub_field('block_30-70__30__text'); ?></div>
								</div>
							</div>
							<div class="content__grid__two-thirds onethirds <?php echo $value2; ?> part-background">
								<div class="content__grid__inner">
									<h6 class="block-title color--white"><?php the_sub_field('block_30-70__70__pre-title'); ?></h6>
									<h3 class="h2 color--pink"><?php the_sub_field('block_30-70__70__title'); ?></h3>
									<div class="preamble color--white"><?php the_sub_field('block_30-70__70__text'); ?></div>
								</div>
							</div>
						</div>

						<?php

        endif;

    endwhile;

else :

    // no layouts found

endif;

?>
