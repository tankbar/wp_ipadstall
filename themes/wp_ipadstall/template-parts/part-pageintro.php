
<?php if( get_field('page_header__background-img') ): ?>
<!-- CONTENT BLOCK INTRO 1 -->



<?php

// vars
$intro = get_field('page_header__intro');

if( $intro ):
	while( have_rows('page_header__intro') ): the_row();


	// vars
	$intro_rub = get_sub_field('intro--rub');
	$intro_text = get_sub_field('intro--text');
	$intro_image = get_sub_field('intro--image');
	$intro_bc = get_sub_field('intro--business-card');
	$intro_offgrid = get_sub_field('intro--off-grid');

	if ($intro_offgrid) :
		$grid = '';
			else :
		$grid = ' on-grid';
			endif

	 ?>
	 <div class="content__inner content__block content__block--intro">
	 	<div class=" content__block__inner">
 			<h2 class="color--blue">
				<?php echo $intro_rub ?></h2>
	 			<?php echo $intro_text ?>
	 	</div>

	 	<div class="offgrid-images<?php echo $grid ?>">
	 		<div class="offgrid-images__inner">
				<?php if( $intro_image ): ?>
	 				<img src="<?php echo $intro_image['url']; ?>">
				<?php endif ?>
				
				<?php
				if( $intro_bc ): ?>
				<div class="contactcard bgcolor--pink">
	 				<div class="contactcard__inner">
		 				<h6 class="block-title color--black"><?php _e( 'Kontaktperson', 'wp_datema' ) ?></h6>
					    <ul>
					    <?php foreach( $intro_bc as $post): // variable must be called $post (IMPORTANT) ?>
					        <?php setup_postdata($intro_bc); ?>
										<h4 class="color--blue"><?php  the_field('bc__for_-och_efternamn'); ?></h4>
						 				<p><a href="tel:<?php the_field('bc__phone'); ?>"><?php the_field('bc__phone'); ?></a><br>
						 				<a href="<?php the_field('bc__email'); ?>"><?php the_field('bc__email'); ?></a></p>
						 				<div class="cta__button  cta__button--white"><?php _e( 'Jag vill bli kontaktad', 'wp_datema' ) ?></div>

										<?php $bc_bild = get_field('bc__bild');?>

										<style type="text/css">
											.offgrid-images .contactcard:before {
												background-image: url('<?php echo $bc_bild['url']; ?>');
											}
										</style>

					    <?php endforeach; ?>
					    </ul>
						</div>
		 			</div>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				<?php endif; ?>
	 		</div>
	 	</div>
	 </div>
	<?php endwhile; ?>

<?php endif; ?>






<div class="clearfix"></div>
<?php endif ?>
