<?php
/**
 * The template for displaying the footer
 *
 *
 *
 * @package Tankbar
 * @subpackage Awesome
 * @since Tankbar Awesome 3.0
 */

get_header();  ?>

<div class="content">

</div>
<!-- END .content -->


<?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	endwhile;
else :
	get_template_part( 'template-parts/content', 'none' );
endif;
?>

<?php get_footer(); ?>
