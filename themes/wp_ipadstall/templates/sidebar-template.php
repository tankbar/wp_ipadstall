<?php 
/*
* Template name: sidebar
*/
get_header(); ?>
<div class="container">
<div class="row">
		<div class="col-sm-6">
		<?php
		if ( have_posts() ) : 
			while ( have_posts() ) : the_post();
				the_content(); 
			endwhile;
		endif; 
		?>
		</div>
		<div class="col-sm-6">
			Sidebar
		</div>
	</div>
</div>

<?php get_footer(); ?>

