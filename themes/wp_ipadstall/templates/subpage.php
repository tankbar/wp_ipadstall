<?php
// Template name: Subpage
?>
<?php get_header(); ?>

<?php

// vars
$field = get_field_object('theme');
$value = $field['value'];
$label = $field['choices'][ $value ];

if ($value == "white"):{
	$headertheme = 'white';
}
endif;

?>

<?php
if ( have_posts() ) :
while ( have_posts() ) : the_post();

		the_content();
endwhile;
endif; ?>


<?php if ($value == "produkt"): ?>

	<div class="swiper-container">

		<div class="iconlist iconlist__swipericons">
				<div class="icon icon__lock active">Säkerhet</div>
				<div class="icon icon__diamond">Kvalitet</div>
				<div class="icon icon__heart">Design</div>
				<div class="icon icon__star">Erfarenhet</div>
				<div class="icon icon__plus">Återförsäljare</div>
		</div>

			<div class="swiper-wrapper">
				<div class="swiper-slide swiper-products">
					<div class="swiper-products__inner" style="background-image: url('/wp-content/themes/wp_ipadstall/assets/images/ipadback.jpg');">
						<div class="swiper-products__inner--text">
							<h2>Låst och laddat</h2>
							<p> Vi har fokuserat på att våra ställ är utformade med låsbara frontramar, dold kabelanslutning samt låssprint i bottenplatta för att fästa eventuell låsanordning allt för att öka säkerheten. Dessutom kan själva iPadenheten låsa ”hemknappen” samt omöjliggöra vidaresurf</p>
						</div>
					</div>
				</div>
				<div class="swiper-slide swiper-products">
					<div class="swiper-products__inner" style="background-image: url('/wp-content/themes/wp_ipadstall/assets/images/ipadfront2-pro.jpg');">
						<div class="swiper-products__inner--text">
							<h2>Kvalitet</h2>
							<p>Våra ställ är utvecklade och producerade i Sverige. De har stabil och hållbar konstruktion, själva iPadenheten är mycket enkel att installera. Dessutom kan USB-kabel för laddning etc löpa inne i själva stället.</p>
						</div>
					</div>
				</div>
				<div class="swiper-slide swiper-products">
					<div class="swiper-products__inner" style="background-image: url('/wp-content/themes/wp_ipadstall/assets/images/ipadstall.jpg');">
						<div class="swiper-products__inner--text">
							<h2>Design</h2>
							<p>Våra iPadställ har en tidlös design som passar i alla miljöer. </p>
						</div>
					</div>
				</div>
				<div class="swiper-slide swiper-products">
					<div class="swiper-products__inner" style="background-image: url('/wp-content/themes/wp_ipadstall/assets/images/ipad-standing.jpg');">
						<div class="swiper-products__inner--text">
							<h2>Erfarenhet</h2>
							<p>Vi har producerat ställ i mer ä tio år och har tagit tagit till oss av alla våra lärdomar för att kunna erbjuda den mest kostnadseffektiva, användbara och säkra lösningen.</p>
						</div>
					</div>
				</div>
				<div class="swiper-slide swiper-products">
					<div class="swiper-products__inner">
						<div class="swiper-products__inner--text">
							<h2>Återförsäljare</h2>
							<p>Vill ni bli återförsäljare för våra ställ har vi en mycket bekväm och lönsam lösning och där all hanteras per automatik från beställning till leverans och där det även finns en supportfunktion.</p>
						</div>
					</div>
				</div>


		</div>
		<!-- Add Pagination -->
		<div class="swiper-pagination"></div>
	</div>



	<!-- Initialize Swiper -->
	<script>
	jQuery( document ).ready(function() {
		var swiper = new Swiper('.swiper-container', {

		});
		});
	</script>





	<div class="twocolboxes">
		<div class="onebox onebox__whitegrad twocolboxes__col1 onebox__inner--margin">
			<div class="shopping-box onebox__inner">

				<div class="onebox__inner__img"><img src="/wp-content/themes/wp_ipadstall/assets/images/280x620-ipadstand-half-pro.png" class="offset offset__bottom offset__bottom--fullheight"></div>

				<div class="shopping-item onebox__inner__info onebox__inner__info--right">

					<div class="product--cat">Golvstativ</div>
					<h4  class="product--title">iPad Pro</h4>
					<div class="product--text">Stabila iPad-golvstativ lackade i vitt med låsbar övre ram samt möjlighet för dold USB-laddkabel i ställröret ner till bottenplattan. Stället har även en låsbygel för att fästa vajer i eller dylikt.</div>


					<div class="product--pris">FR. <span>5.795 kr</span></div>

					<a href="#" class="cta__button cta__button--green">Vidare</a>

				</div>

			</div>
		</div>

		<div class="onebox onebox__whitegrad twocolboxes__col2">
			<div class="shopping-box onebox__inner">

				<div class="onebox__inner__img"><img src="/wp-content/themes/wp_ipadstall/assets/images/280x620-ipadstand2-half-pro.png"></div>

				<div class="shopping-item onebox__inner__info onebox__inner__info--right">

					<div class="product--cat">Vägg/Bordstativ</div>
					<h4  class="product--title">iPad Pro</h4>
					<div class="product--text">Stabila iPad-vägg-/bordsstativ lackade i vitt med låsbar övre ram samt möjlighet för dold USB-laddkabel i ställröret till monteringsplattan</div>


					<div class="product--pris">FR. <span>5.795 kr</span></div>

					<a href="#" class="cta__button cta__button--green">Vidare</a>

				</div>

			</div>
		</div>
	</div>

	<div class="twocolboxes">
		<div class="onebox onebox__whitegrad twocolboxes__col1 onebox__inner--margin">
			<div class="shopping-box onebox__inner">

				<div class="onebox__inner__img"><img src="/wp-content/themes/wp_ipadstall/assets/images/280x620-ipadstand-half.png" class="offset offset__bottom offset__bottom--fullheight"></div>

				<div class="shopping-item onebox__inner__info onebox__inner__info--right">

					<div class="product--cat">Golvstativ</div>
					<h4  class="product--title">iPad 9,7"</h4>
					<div class="product--text">Stabila iPad-golvstativ lackade i vitt med låsbar övre ram samt möjlighet för dold USB-laddkabel i ställröret ner till bottenplattan. Stället har även en låsbygel för att fästa vajer i eller dylikt.</div>


					<div class="product--pris">FR. <span>4.795 kr</span></div>

					<a href="#" class="cta__button cta__button--green">Vidare</a>

				</div>

			</div>
		</div>

		<div class="onebox onebox__whitegrad twocolboxes__col2">
			<div class="shopping-box onebox__inner">

				<div class="onebox__inner__img"><img src="/wp-content/themes/wp_ipadstall/assets/images/280x620-ipadstand2-half.png"></div>

				<div class="shopping-item onebox__inner__info onebox__inner__info--right">

					<div class="product--cat">Vägg/Bordstativ</div>
					<h4  class="product--title">iPad 9,7"</h4>
					<div class="product--text">Stabila iPad-vägg-/bordsstativ lackade i vitt med låsbar övre ram samt möjlighet för dold USB-laddkabel i ställröret till monteringsplattan</div>

					<div class="product--pris">FR. <span>4.795 kr</span></div>

					<a href="#" class="cta__button cta__button--green">Vidare</a>

				</div>

			</div>
		</div>
	</div>


<?php endif; ?>







<?php get_footer(); ?>
