<?php
// Template name: Contact
?>
<?php get_header(); ?>
<div class="contact-page">
    <div class="intro">
      <h1>Kontakta oss</h1>
      <div class="preamble">Frågor eller funderingar?<br>
        Hör av dig!</div>

        <p><h4>Telefon: <span style="font-weight: 100"><a href="tel:086546500" style="text-decoration: none">08-654 65 00</a></span></h4>
        <p><h4>E-post: <span style="font-weight: 100"><a href="mailto:info@mediascreen.nu">info@mediascreen.nu</a></span></h4>

        <p>  Mediascreen finns i <strong>Stockholm</strong>, <strong>Nyköping</strong> och <strong>Göteborg</strong></p>
    </div>
    <div class="contact-form">
      <div class="contact-form__inner">
      <?php gravity_form( 1, false, false, false, '', false ); ?>
      </div>
    </div>

</div>
<?php get_footer(); ?>
