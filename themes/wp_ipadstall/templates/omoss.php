<?php
// Template name: Omoss
?>
<?php get_header(); ?>

<div class="cols">
	<div class="cols__col1">

	<?php
	if ( have_posts() ) :
	while ( have_posts() ) : the_post();

			the_content();
	endwhile;
	endif; ?>

	</div>
	<div class="cols__col2">
	<h4>Populära användningsområden</h4>
	<ul class="checklist">
			<li>Erbjudanden och information</li>
			<li>Aktiviteter och program</li>
			<li>Spel</li>
			<li>Interaktiva kartor</li>
			<li>Kundundersökningar och enkäter</li>
			<li>Anmälan till nyhetsbrev</li>
			<li>Mäklarobjekt med bilder och planlösningar</li>
			<li>Kommande visningar</li>
			<li>Exponering av samarbetspartners</li>
			<li>Restaurangmenyer</li>
			<li>Bilar till försäljning</li>
			<li>Aktuell boränta</li>
			<li>Finansieringslösningar</li>
	</ul>
	</div>
<div>

<?php get_footer(); ?>
