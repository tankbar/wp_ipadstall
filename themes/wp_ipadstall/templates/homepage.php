<?php
// Template name: Homepage
?>
<?php get_header(); ?>


<div class="content homepage">
	<div class="content__inner">
		<div class="twocolboxes">
			<div id="col1" class="onebox onebox__gray twocolboxes__col1 onebox__inner--margin">
				<div class="onebox__inner">
					<img src="/wp-content/themes/wp_ipadstall/assets/images/280x620-ipadstand.png" alt="iPadställ för golv" class="offset offset__top img-left">
					<div class="onebox__inner__info  onebox__inner__info--right onebox__inner__info--center">
						<div class="product--cat"><?php _e( 'iPad-ställ', 'ipadstall' ); ?></div>
						<h3  class="product--title"><?php _e( 'För golv', 'ipadstall' ); ?></h3>
						<div class="product--text"><?php _e( 'Lackade iPad-golvstativ i vitt, silver eller svart samt med vid <i>Solid Surface</i>-topp. Passar iPad Pro och senaste iPad.', 'ipadstall' ); ?></div>
						<div class="icon icon--ipad"><span class="ipad-pro"></span><?php _e( 'iPad 12,9"', 'ipadstall' ); ?></div>
						<div class="icon icon--ipad"><span class="ipad"></span><?php _e( 'iPad 10,2"', 'ipadstall' ); ?></div>
					</div>
				</div>
			</div>
			<div id="col2" class="onebox onebox__gray twocolboxes__col2">
				<div class="onebox__inner">
					<div class="onebox__inner__info onebox__inner__info--left onebox__inner__info--center">
						<div class="product--cat"><?php _e( 'iPad-ställ', 'ipadstall' ); ?></div>
						<h3  class="product--title"><?php _e( 'För vägg/bord', 'ipadstall' ); ?></h3>
						<div class="product--text"><?php _e( 'iPad-stativ för vägg eller bord i vitt, silver eller svart samt med vit <i>Solid Surface</i>-topp.  Finns för iPad Pro och senaste iPad.', 'ipadstall' ); ?></div>
						<div class="icon icon--ipad"><span class="ipad-pro"></span><?php _e( 'iPad 12,9"', 'ipadstall' ); ?></div>
						<div class="icon icon--ipad"><span class="ipad"></span><?php _e( 'iPad 10,2"', 'ipadstall' ); ?></div>
					</div>
					<img src="/wp-content/themes/wp_ipadstall/assets/images/280x620-ipadstand2.png" alt="iPadställ för vägg/bord" class="offset offset__top img-right">
				</div>
			</div>
		</div>
		<div class="twocolboxes">
			<div class="onebox onebox__whitegrad twocolboxes__col1 onebox__inner--margin">
				<div class="shopping-box onebox__inner">
					<div class="onebox__inner__img"><img src="/wp-content/themes/wp_ipadstall/assets/images/280x620-ipadstand-half-pro.png" alt="Golvstativ iPad Pro" class="offset offset__bottom offset__bottom--fullheight"></div>
					<div class="shopping-item onebox__inner__info onebox__inner__info--right">
						<div class="product--cat"><?php _e( 'Golvstativ', 'ipadstall' ); ?></div>
						<h4  class="product--title"><?php _e( 'iPad 12,9"', 'ipadstall' ); ?></h4>
						<div class="product--text"><?php _e( 'Stabila iPad-golvstativ lackade i vitt med låsbar övre ram samt möjlighet för dold USB-laddkabel i ställröret ner till bottenplattan. Stället har även en låsbygel för att fästa vajer i eller dylikt.', 'ipadstall' ); ?></div>
						<div class="product--pris"><?php _e( 'FR. <span>2 970 kr</span>', 'ipadstall' ); ?></div>
						<a href="/bestall-ipadstall?qnt1=1&qnt2=0&qnt3=0&qnt4=0" class="cta__button cta__button--green"><?php _e( 'Beställ iPadställ', 'ipadstall' ); ?></a>
					</div>
				</div>
			</div>
			<div class="onebox onebox__whitegrad twocolboxes__col2">
				<div class="shopping-box onebox__inner">
					<div class="onebox__inner__img"><img src="/wp-content/themes/wp_ipadstall/assets/images/280x620-ipadstand2-half-pro.png" alt="Vägg/bordsstativ iPad Pro" ></div>
					<div class="shopping-item onebox__inner__info onebox__inner__info--right">
						<div class="product--cat"><?php _e( 'Vägg/Bordstativ', 'ipadstall' ); ?></div>
						<h4  class="product--title"><?php _e( 'iPad 12,9"', 'ipadstall' ); ?></h4>
						<div class="product--text"><?php _e( 'Stabila iPad-vägg-/bordsstativ lackade i vitt med låsbar övre ram samt möjlighet för dold USB-laddkabel i ställröret till monteringsplattan', 'ipadstall' ); ?></div>
						<div class="product--pris"><?php _e( 'FR. <span>2 450 kr</span>', 'ipadstall' ); ?></div>
						<a href="/bestall-ipadstall?qnt1=0&qnt2=0&qnt3=1&qnt4=0" class="cta__button cta__button--green"><?php _e( 'Beställ iPadställ', 'ipadstall' ); ?></a>
					</div>
				</div>
			</div>
		</div>
		<div class="twocolboxes">
			<div class="onebox onebox__whitegrad twocolboxes__col1 onebox__inner--margin">
				<div class="shopping-box onebox__inner">
					<div class="onebox__inner__img"><img src="/wp-content/themes/wp_ipadstall/assets/images/280x620-ipadstand-half.png" alt="Golvstativ iPad 9,7"  class="offset offset__bottom offset__bottom--fullheight"></div>
					<div class="shopping-item onebox__inner__info onebox__inner__info--right">
						<div class="product--cat"><?php _e( 'Golvstativ', 'ipadstall' ); ?></div>
						<h4  class="product--title"><?php _e( 'iPad 10,2"', 'ipadstall' ); ?></h4>
						<div class="product--text"><?php _e( 'Stabila iPad-golvstativ lackade i vitt med låsbar övre ram samt möjlighet för dold USB-laddkabel i ställröret ner till bottenplattan. Stället har även en låsbygel för att fästa vajer i eller dylikt.', 'ipadstall' ); ?></div>
						<div class="product--pris"><?php _e( 'FR. <span>2 870 kr</span>', 'ipadstall' ); ?></span></div>
						<a href="/bestall-ipadstall?qnt1=0&qnt2=1&qnt3=0&qnt4=0" class="cta__button cta__button--green"><?php _e( 'Beställ iPadställ', 'ipadstall' ); ?></a>
					</div>
				</div>
			</div>
			<div class="onebox onebox__whitegrad twocolboxes__col2">
				<div class="shopping-box onebox__inner">
					<div class="onebox__inner__img"><img src="/wp-content/themes/wp_ipadstall/assets/images/280x620-ipadstand2-half.png"  alt="Vägg/bordsstativ iPad 9,7"></div>
					<div class="shopping-item onebox__inner__info onebox__inner__info--right">
						<div class="product--cat"><?php _e( 'Vägg/Bordstativ', 'ipadstall' ); ?></div>
						<h4  class="product--title"><?php _e( 'iPad 10,2"', 'ipadstall' ); ?></h4>
						<div class="product--text"><?php _e( 'Stabila iPad-vägg-/bordsstativ lackade i vitt med låsbar övre ram samt möjlighet för dold USB-laddkabel i ställröret till monteringsplattan', 'ipadstall' ); ?></div>
						<div class="product--pris"><?php _e( 'FR. <span>2 160 kr</span>', 'ipadstall' ); ?></div>
						<a href="/bestall-ipadstall?qnt1=0&qnt2=0&qnt3=0&qnt4=1" class="cta__button cta__button--green"><?php _e( 'Beställ iPadställ', 'ipadstall' ); ?></a>
					</div>
				</div>
			</div>
		</div>
        <?php if( have_rows('slider') ): ?>
        <div id="usp" class="swiper-container">
            <div class="swiper-wrapper">
            <?php while( have_rows('slider') ): the_row();
                // vars
                $image = get_sub_field('swiper__bakimage');
                $title = get_sub_field('swiper__title');
                $preamble = get_sub_field('swiper__preamble');
                ?>
                <div class="swiper-slide swiper-products">
                    <div class="swiper-products__inner" style="background-image: url('<?php echo $image['url']; ?>');">
                        <div class="swiper-products__inner--text">
                            <h2><?php echo $title; ?></h2>
                            <p><?php echo $preamble; ?></p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
        <?php endif; ?>
        <!-- Initialize Swiper -->
        <script>
            jQuery( document ).ready(function() {
                var swiper = new Swiper('.swiper-container', {
                    autoplay: {
                        delay: 5000,
                    },
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true,
                        renderBullet: function (index, className) {
                            return '<div class="' + className + '"><span class="item' + (index + 1) + '"></span></div>';
                        },
                    },
                });
            });
        </script>
        <?php
        $reseller = get_field('reseller');
        if( $reseller ):  ?>
        <div class="contentbox contentbox__purplegrad">
            <div class="contentbox__inner">
                <h3><?php echo $reseller['reseller__title']; ?></h3>
                <div class="text"><?php echo $reseller['reseller__preamble']; ?></div>
                <a href="<?php echo $reseller['reseller__cta']['url']; ?>" class="cta__button cta__button--green cta__button--center"><?php echo $reseller['reseller__cta']['title']; ?></a>
            </div>
        </div>
        <?php
        endif; ?>
	</div>
</div>
<?php get_footer(); ?>