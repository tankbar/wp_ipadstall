<?php


get_header(); ?>




<div class="bloglist">
	<div class="container">
		<div class="page_header">
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12">
			<?php
				$i = 0;
				if (have_posts()):
					while (have_posts()): the_post();
						$size = ($i++ == 0) ?  '1col-sm-4 col-md-6' : '2col-sm-4 col-md-3';
						echo '<div class="single-post ' . $size . '">';
						get_template_part('template-parts/list', get_post_format());
						echo '</div>';
					endwhile;
				else:
					get_template_part('template-parts/content', 'none');
				endif;
			?>
			</div>
		</div>
</div>
</div>



<?php get_footer(); ?>
