<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package Tankbar
 * @subpackage Awesome
 * @since Tankbar Awesome 3.0
 */

get_header(); ?>
<section class="error-404 not-found">
	<header class="page-header">
		<h1 class="page-title"><?php _e( 'Vi kunde inte hitta den här sidan.', 'ipadstall' ); ?></h1>
	</header>
	<div class="page-content">
		<p>
            <a href="/"><?php _e( 'Gå till startsidan', 'ipadstall' ); ?></a>
		</p>
	</div>
</section>
<?php get_footer(); ?>
