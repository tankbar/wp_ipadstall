<?php
/**
 * The template for displaying the posts
 *
 *
 *
 * @package Tankbar
 * @subpackage Awesome
 * @since Tankbar Awesome 3.0
 */

$categories = get_categories( array(
	'orderby' => 'name',
	'parent'  => 0
) );

get_header();  ?>

				<?php
					if (have_posts()):
						while (have_posts()): the_post();
							get_template_part('template-parts/list', get_post_format());
						endwhile;
					else:
						get_template_part('template-parts/content', 'none');
					endif;
				?>

<?php get_footer(); ?>
