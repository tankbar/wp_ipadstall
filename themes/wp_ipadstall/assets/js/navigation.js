jQuery('document').ready(function onDocumentReadyNavigation($) {
	var $burger = $('.mobile-menu__burger');
	var $menu = $('.mobile-menu');
	var $body = $('body');

	$burger.on('click', onBurgerClick);
	$('.js-toggle-mobile-menu').on('click', onBurgerClick);

	function onBurgerClick() {
		if($body.hasClass('mobile-search--search-open')) {
			$body.trigger('searchclose');
			$body.removeClass('mobile-search--search-open');
		} else if($body.hasClass('mobile-menu--open')) {
			$body.removeClass('mobile-menu--open');
		} else {
			$body.addClass('mobile-menu--open');
			$menu.scrollTop(0);
		}
	}

});
