<?php
/**
 * The template for displaying the header
 *
 *
 * @package Tankbar
 * @subpackage Awesome
 * @since Tankbar Awesome 3.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="7b2YSSP5X3GPjJ8Y_aZ9z3AT13W1AUfne3bK_hYYZfA" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<title itemprop="name"><?php
	if ( defined('WPSEO_VERSION') ) {
		wp_title('');
	} else {
		bloginfo('name'); ?> <?php wp_title(' - ', true);
	}
    ?></title>
 	<?php wp_head(); ?>
	<?php tb_meta(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page-wrapper" class="page__wrapper"><?php // #page-wrapper - START ?>
	<?php tb_before_header(); ?>
	<header id="site-header" class="header header--primary light" role="banner">
		<div class="header__wrapper">

            <?php if ( is_front_page()){ ?>
            <video autoplay muted loop id="background-video">
                <source src="/wp-content/themes/wp_ipadstall/assets/images/ipadstall3.mp4" type="video/mp4">
            </video>

                <?php
            }
            ?>

			<div class="header__wrapper__inner">
				<a class="header__logo" href="<?php echo get_site_url(); ?>">iPadStäll</a>
				<?php tb_header(); ?>
				<div id="menu" class="js-toggle-mobile-menu"></div>
			</div>
			<?php
			if ( is_front_page()){
			// Static homepage

            $header = get_field('header');
            if( $header ):  ?>
                <div class="introtext">
                    <h1><?php echo $header['header__title']; ?></h1>
                    <div class="preamble"><?php echo $header['header__preamble']; ?></div>
                    <a href="<?php echo $header['header__cta']['url']; ?>" class="cta__button cta__button--trans  cta__button--center"><?php echo $header['header__cta']['title']; ?></a>
                </div>
                <div class="iconlist">
                    <a href="#usp" class="icon icon__lock">Säkerhet</a>
                    <a href="#usp" class="icon icon__diamond">Kvalitet</a>
                    <a href="#usp" class="icon icon__heart">Design</a>
                    <a href="#usp" class="icon icon__star">Erfarenhet</a>
                    <a href="#usp" class="icon icon__plus">Återförsäljare</a>
                </div>
               <?php
                endif;
                ?>
			<?php
			}
			?>
		</div>
		<?php tb_header_after_inner(); ?>
	</header>
	<?php tb_after_header(); ?>
	<section id="main"> <?php // Closed in footer ?>
