<?php

function tb_third_nav() {
  $args = array(
    'theme_location'  => 'third',
    'container'	=> 'nav',
    'container_class' => 'nav-desktop nav-desktop--secondary',
    'echo'				=> true
  );

  if ( has_nav_menu( 'third' ) ) {
    wp_nav_menu($args);
  }
}

function tb_secondary_nav() {
  $args = array(
    'theme_location'  => 'secondary',
    'container'	=> 'nav',
    'container_class' => 'nav-desktop nav-desktop--secondary',
    'echo'				=> true
  );

  if ( has_nav_menu( 'secondary' ) ) {
    wp_nav_menu($args);
  }
}

function tb_primary_nav() {
  $args = array(
    'theme_location'  => 'primary',
    'container'	=> 'nav',
    'container_class' => '',
    'echo'				=> true
  );

  if ( has_nav_menu( 'primary' ) ) {
    wp_nav_menu($args);
  }
}

add_action('tb_header','tb_mobile_nav', 3);
function tb_mobile_nav() {
  $primary = array(
    'theme_location'  => 'primary',
    'container'	      => 'nav',
    'container_class' => 'header__navigation',
    'echo'				    => true,
  );

  $secondary = array(
    'theme_location'  => 'secondary',
    'container'	      => 'nav',
    'container_class' => 'header__navigation--secondary',
    'echo'				    => true
  );

  $third = array(
    'theme_location'  => 'third',
    'container'	      => 'nav',
    'container_class' => 'mobile-menu__nav mobile-menu__nav--third',
    'echo'				    => true
  );


  if(!has_nav_menu('primary') && !has_nav_menu('secondary'))
    return;
?>

      <?php

if(has_nav_menu('secondary')):
  wp_nav_menu($secondary);
endif;

if(has_nav_menu('primary')):
  wp_nav_menu($primary);
endif;



      ?>

  <?php
}

add_action('tb_meta', 'google_tag_manager_head', 20);
function google_tag_manager_head() { ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W2Q2VPJ');</script>
    <!-- End Google Tag Manager -->
<?php }

add_action('tb_before_header', 'google_tag_manager_body', 10);
function google_tag_manager_body() { ?>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W2Q2VPJ"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


<?php }

?>
