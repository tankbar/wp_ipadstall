<?php


add_action('tb_footer', 'footer_layout');
function footer_layout() {
    ?>

    <div class="footer__inner__col footer__inner__col__c1">
        <a href="<?php echo get_site_url(); ?>" class="footer__inner__col footer__inner__col__c1--logo"></a>

        <div class="footer__inner__col footer__inner__col__c1--logo-mediascreen-text">En del av</div>
        <a href="https://www.mediascreen.nu/" class="footer__inner__col footer__inner__col__c1--logo-mediascreen">Mediascreen</a>




    </div>
    <div class="footer__inner__col footer__inner__col__c2">
        <?php dynamic_sidebar('footer1'); ?>
    </div>
    <div class="footer__inner__col footer__inner__col__c3">
        <?php dynamic_sidebar('footer2'); ?>
    </div>
    <div class="footer__inner__col footer__inner__col__c4">
        <?php dynamic_sidebar('footer3'); ?>
    </div>
    <div class="footer__inner__col footer__inner__col__c5">
        <?php dynamic_sidebar('footer4'); ?>
    </div>

    <?php

}
