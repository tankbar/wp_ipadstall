<?php
// Register menus
register_nav_menus( array(
  'primary' => __( 'Huvudmeny',      'webbyra' ),
  'secondary'  => __( 'Sekundär meny', 'webbyra' ),
    'third'  => __( 'Tredje meny', 'webbyra' ),
  'footer-links'  => __( 'Footer meny', 'webbyra' ),
  'social-menu' => __( 'Social meny', 'webbyra')
));
