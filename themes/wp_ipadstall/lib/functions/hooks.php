<?php
function tb_meta() {
  do_action('tb_meta');
}

function tb_before_header() {
  do_action( 'tb_before_header' );
}

function tb_header() {
  do_action('tb_header');
}

function tb_header_after_inner() {
  do_action('tb_header_after_inner');
}

function tb_after_header() {
  do_action( 'tb_after_header' );
}

function tb_before_content() {
  do_action( 'tb_before_content' );
}

function tb_content() {
  do_action( 'tb_content' );
}

function tb_after_content() {
  do_action( 'tb_after_content' );
}

function tb_before_footer() {
  do_action( 'tb_before_footer' );
}

function tb_footer() {
  do_action('tb_footer');
}

function tb_after_footer() {
  do_action( 'tb_after_footer' );
}