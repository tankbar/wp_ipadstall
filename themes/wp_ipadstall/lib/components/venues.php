<?php
function venues() {
?>

<?php if( get_field('show_venues') ): ?>

<div id="venues">

  <div class="inner">
    <div class="venues__left_col">
      <ul class="iconlist">
          <li class="icon icon--butikskedjor">Butikskedjor och retail</li>
          <li class="icon icon--hotell">Hotell och konferens</li>
          <li class="icon icon--kommuner">Kommuner och turism</li>
          <li class="icon icon--skolor">Skolor och utbildning</li>
          <li class="icon icon--kopcentrum">Köpcentrum och terminaler</li>
      </ul>
    </div>
    <div class="venues__right_col">
      <ul class="iconlist">
          <li class="icon icon--arenor">Arenor och event</li>
          <li class="icon icon--fastighetsmaklare">Fastighetsmäklare</li>
          <li class="icon icon--bilhallar">Bilhallar</li>
          <li class="icon icon--halsovard">Hälsovård och sjukhus</li>
          <li class="icon icon--fastigehetsagare">Fastighetsägare</li>
      </ul>
    </div>
  </div>

<button class="btn btn-pink btn-center">Kontakta oss</button>

</div>

<?php endif; ?>
<?php
}
