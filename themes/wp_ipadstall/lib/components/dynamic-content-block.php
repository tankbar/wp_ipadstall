<?php
function dynamiccontentblock() {
?>


<div class="content mt-large mb-large">
  <h2 style="max-width: 480px;">Vad vi pratar om när vi pratar webb.</h2>
  <div class="content__inner">
  <div class="content__inner__col-1-3">
      <p>Lorem ipsum dolor amet next level prism microdosing, 90's before they sold out shaman unicorn PBR&B raclette try-hard yuccie
      sustainable intelligentsia single-origin coffee seitan. Next level ramps PBR&B adaptogen kitsch lomo, freegan 8-bit vape.
      Forage selvage tumeric, fashion axe flexitarian banjo heirloom williamsburg knausgaard pitchfork adaptogen organic green juice vexillologist.
      Lyft woke cliche food truck 90's ennui drinking vinegar cardigan banh mi waistcoat literally knausgaard leggings.</p>

      <p>Beard everyday carry blue bottle XOXO microdosing tilde bitters whatever gastropub man bun. Intelligentsia put a bird on it fashion axe, butcher
      lyft iceland squid. Subway tile humblebrag hell of bushwick helvetica. Salvia heirloom helvetica mumblecore waistcoat. Art party wolf disrupt cliche
      succulents roof party chillwave synth lomo drinking vinegar freegan bitters semiotics. Forage cold-pressed semiotics salvia chia YOLO.</p>
  </div>
  <div class="content__inner__col-2-3">

    <ul>
        <li>
          <strong>1996</strong>
          <p>Vi startade webbyrå</p>
        </li>
        <li>
          <strong>1998</strong>
 							<p>Lorem ipsum dolor<br> 
            Forage selvage tumeric <br>
            Forage selvage tumeric</p>
        </li>
        <li>
          <strong>2000</strong>
          <p>Lorem ipsum dolor<br>
 Forage selvage tumeric<br>
 Forage selvage tumeric</p>
        </li>
        <li>
          <strong>2003</strong>
          <p>Lorem ipsum dolor <br>
            Forage selvage tumeric</p>
        </li>
  </ul>

  </div>


  <div class="content__inner__col-3-3">


    <ul>
        <li>
          <strong>2005 </strong>
          <p>Lorem ipsum dolor</p>
        </li>
        <li>

          <strong>2010</strong>
          <p> Lorem ipsum dolor<br>
 Forage selvage tumeric<br>
 Forage selvage tumeric</p>
          </li>
          <li>

          <strong>2013 </strong>
          <p>Lorem ipsum dolor <br>
          Forage selvage tumeric</p>
          </li>
          <li>

          <strong>2018 </strong>
          <p>Lorem ipsum dolor<br> Forage selvage tumeric <br>
            Forage selvage tumeric</p>
          </li>

        </li>
        </ul>
      </div>
    </div>
    <div class="buttonrow">
        <div class="cta__button cta__button--blue">Dela</div>
        <div class="cta__button cta__button--green">Donera</div>
    </div>

  </div>



<?php
}
