<?php
function articleswiper() {
?>



<?php
$args = array(
  'post_type'   => 'swiperpost',
  'post_status' => 'publish',
  'tax_query'   => array(
    array(
      'taxonomy' => 'swiperpost_categories',
      'field'    => 'slug',
      'terms'    => 'webbtjanster'
    )
  )
 );
$webbtjanster = new WP_Query( $args );
if( $webbtjanster->have_posts() ) :
?>


<!-- Webbtjänster Start -->



<?php if ('alt2' !== get_field('subpage__postswiper')) { ?>

<div class="swipercontent__inner swipercontent__block">
  <h4><span class="blue icon-list icon-list__services">Webbtjänster</span> <span class="sep">det du behäver veta</span> <span class="sep">förklarat på ren svenska.</span></h4>





  <div class="articles swiper-container" >
    <div class="articles__inner swiper-wrapper" >

      <?php
        while( $webbtjanster->have_posts() ) :
          $webbtjanster->the_post();

        $image = get_field('swipertitle__backgroundimage');

        if( !empty($image) ): ?>
          <?php $imageurl = $image['url']; ?>
        <?php endif; ?>

      <div class="articles__inner__col swiper-slide"  onclick="location.href = '<?php the_field('swiperpost__link');?>';">

        <?php
        if ('1' == get_field('swiperpost__new')) { ?>
          <div class="articles__inner--new"></div>
        <?php } ?>

        <div class="articles__inner__col__article parallax-bg" >
          <div class="articles__inner__col__article--inner">

            <div class="bg-inner" style="background-image:url('<?php echo $imageurl ?>');"   data-swiper-parallax="6%"></div>
            <div class="desc desc--blue"   style="  transform: translate3d(0, 0px, 0px); transition-duration: 0ms;">
              <h3><?php the_field('swiperpost__title');?></h3>
              <div class="meta">955 visningar

              <span class="icon icon__comments">16</span>
              <span class="icon icon__heart">269</span>
              </div>

            </div>
          </div>
        </div>
      </div>

      <?php
    endwhile; ?>

      </div>
    </div>

    <?php

    $imageurl = "";

    wp_reset_postdata();
  ?><?php }?>
<?php
else :
esc_html_e( 'Innehåll saknas', 'text-domain' );
endif;
?>
</div>
<!-- End Webbtjänster -->



<!-- Webbtjänster Start -->

<?php if ('alt1' !== get_field('subpage__postswiper')) { ?>

<div class="swipercontent__inner swipercontent__block">
  <h4><span class="pink icon-list icon-list__document">Dokument</span> <span class="sep">användbara mallar och verktyg</span> <span class="sep">hämta dem gratis.</span></h4>


  <?php
  $args = array(
    'post_type'   => 'swiperpost',
    'post_status' => 'publish',
    'tax_query'   => array(
      array(
        'taxonomy' => 'swiperpost_categories',
        'field'    => 'slug',
        'terms'    => 'dokument'
      )
    )
   );
  $webbtjanster = new WP_Query( $args );
  if( $webbtjanster->have_posts() ) :
  ?>


  <div class="articles swiper-container" >
    <div class="articles__inner swiper-wrapper" >

      <?php
        while( $webbtjanster->have_posts() ) :
          $webbtjanster->the_post();

        $image = get_field('swipertitle__backgroundimage');

        if( !empty($image) ): ?>
          <?php $imageurl = $image['url']; ?>
        <?php endif; ?>

      <div class="articles__inner__col swiper-slide"  onclick="location.href = '<?php the_field('swiperpost__link');?>';">

  		    <?php
          if ('1' == get_field('swiperpost__new')) { ?>
            <div class="articles__inner--new"></div>
      		<?php } ?>


        <div class="articles__inner__col__article parallax-bg" >
          <div class="articles__inner__col__article--inner">

            <div class="bg-inner" style="background-image:url('<?php echo $imageurl ?>');"   data-swiper-parallax="6%"></div>
            <div class="desc desc--pink"   style="  transform: translate3d(0, 0px, 0px); transition-duration: 0ms;">
              <h3><?php the_field('swiperpost__title');?></h3>
              <div class="meta">955 visningar

              <span class="icon icon__comments">16</span>
              <span class="icon icon__heart">269</span>
              </div>

            </div>
          </div>
        </div>
      </div>

      <?php
    endwhile; ?>

      </div>
    </div>

    <?php
    wp_reset_postdata();
  ?>
<?php
else :
esc_html_e( 'Innehåll saknas', 'text-domain' );
endif;
?>
</div>
<?php }?>


<!-- End Dokument -->



<!-- Initialize Swiper -->
<script>
jQuery( document ).ready(function() {
var swiper = new Swiper('.articles', {
slidesPerView: 7,
spaceBetween: 30,
parallax: true,
speed: 600,
breakpointsInverse: true,
breakpoints: {

    420: {
      slidesPerView: 1,
      spaceBetween: 30
    },

      720: {
        slidesPerView: 2,
        spaceBetween: 20
      },

      1024: {
        slidesPerView: 2,
        spaceBetween: 20
      },

      1200: {
        slidesPerView: 3,
        spaceBetween: 30
      },

      1600: {
        slidesPerView: 4,
        spaceBetween: 30
      },

      1900: {
        slidesPerView: 5,
        spaceBetween: 30
      }
    }
  });
});
</script>


<?php
}
