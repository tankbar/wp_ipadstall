<?php
function newsletterform() {
?>

<div class="content">
    <div class="content__inner">


    		<!-- Begin MailChimp Signup Form -->
    <div id="mc_embed_signup">

      <h4><span>Nyhetsbrev</span> - Häng med i flödet</h4>

        <form action="https://tankbar.us10.list-manage.com/subscribe/post?u=cfe31aed66e4e6ccb8f1e531e&amp;id=dcb9e8d709" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">

        		<input type="text" value=""  placeholder="Förnamn"  name="FNAME" class="" id="mce-FNAME">
        		<input type="text" value=""  placeholder="Efternamn"  name="LNAME" class="" id="mce-LNAME">
        		<input type="email" value="" placeholder="E-postadress" name="EMAIL" class="required email" id="mce-EMAIL">

          <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_cfe31aed66e4e6ccb8f1e531e_dcb9e8d709" tabindex="-1" value=""></div>
            <input type="submit" value="Skicka" name="subscribe" id="mc-embedded-subscribe" class="button">



        				<div class="response" id="mce-error-response" style="display:none"></div>
        				<div class="response" id="mce-success-response" style="display:none"></div>

            </div>
        </form>

        		<span style="font-size: 0.7em; top: 30px; position: relative; color: red;">[OBS! OVAN ÄR TILL MAILCHIMP FÖR TANKBAR.COM ]</span>

        </div>

        <!--End mc_embed_signup-->

        </div>
    </div>

<div class="gray_divid"></div>

<?php
}
