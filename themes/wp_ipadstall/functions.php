<?php

// Include functions
include ( get_template_directory() . '/lib/functions/hooks.php' );
include ( get_template_directory() . '/lib/functions/disable-comments.php' );
include ( get_template_directory() . '/lib/functions/remove-emojis.php' );

// Include layout
include ( get_template_directory() . '/lib/layout/header.php');
include ( get_template_directory() . '/lib/layout/navigation.php');
include ( get_template_directory() . '/lib/layout/footer.php');
include ( get_template_directory() . '/lib/layout/widgets.php');



// Styles
function tb_styles() {
	wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'.'','');
	wp_enqueue_style('main-css', get_template_directory_uri() . '/assets/dist/css/main.min.css','','');


	wp_enqueue_style('typekit', 'https://use.typekit.net/cqr0wbj.css','','');


}
add_action( 'wp_enqueue_scripts', 'tb_styles' );

function tb_register_scripts() {
	wp_register_script('global-scripts', get_template_directory_uri() . '/assets/dist/js/global.js', array('jquery'), '', true);
	wp_register_script('global-lib-scripts', get_template_directory_uri() . '/assets/dist/js/global-lib.js', array('jquery'), '', true);

}
add_action('init', 'tb_register_scripts');

// Scripts
function tb_scripts() {
	wp_enqueue_script('global-scripts');
	wp_enqueue_script('global-lib-scripts');
	wp_enqueue_script('extrn-scripts');
}
add_action( 'wp_enqueue_scripts', 'tb_scripts');

// Admin style
add_action('admin_enqueue_scripts', 'admin_theme_styles');
function admin_theme_styles() {
	wp_enqueue_style('admin-css', get_template_directory_uri() . '/lib/admin/css/admin-style.css','','');
}

add_action('login_enqueue_scripts', 'login_styles');
function login_styles() {
	wp_enqueue_style('login-css', get_template_directory_uri() . '/lib/admin/css/login-style.css','','');
}


/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 18;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );



// Favicons http://www.favicon-generator.org/

//add_action('tb_meta', 'tb_favicons');
function tb_favicons() {
?>
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/favicons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
<?php
}
