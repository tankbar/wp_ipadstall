<?php
/**
 * The template for displaying the footer
 *
 *
 * @package Tankbar
 * @subpackage Awesome
 * @since Tankbar Awesome 3.0
 */

?>
</section> <?php //Close #main section ?>
<?php tb_before_footer(); ?>

    <div id="pre-footer" class="pre-footer">
        <div class="pre-footer__inner">
            <div class="logorow">
                <span><img src="/wp-content/themes/wp_ipadstall/assets/images/logo-1.jpg" alt="Intersport logotyp"></span>
                <span><img src="/wp-content/themes/wp_ipadstall/assets/images/logo-2.jpg" alt="Fastighetsägarna logotyp"></span>
                <span><img src="/wp-content/themes/wp_ipadstall/assets/images/logo-3.jpg" alt="Skandia Mäklarna logotyp"></span>
                <span><img src="/wp-content/themes/wp_ipadstall/assets/images/logo-4.jpg" alt="ICA logotyp"></span>
                <span><img src="/wp-content/themes/wp_ipadstall/assets/images/logo-5.jpg" alt="Atlas Copco logotyp"></span>
                <span><img src="/wp-content/themes/wp_ipadstall/assets/images/logo-6.jpg" alt="Craft logotyp"></span>
            </div>
        </div>
    </div>

	<footer id="footer" class="site-footer footer" role="contentinfo">
	<div class="footer__inner">
		<?php tb_footer(); ?>
	</div>
	</footer>
	<div id="sub-footer" class="sub-footer">
		<div class="sub-footer__inner">
            <p><a href="tel:086546500">08-654 65 00</a> - <a href="tel:0155264900">0155-26 49 00</a> - <a href="mailto:info@mediascreen.nu">info@mediascreen.nu</a></p>
		</div>
		<?php tb_after_footer(); ?>
		<?php wp_footer(); ?>
	</div>
</div><?php // #page-wrapper - END ?>

</body>
</html>
