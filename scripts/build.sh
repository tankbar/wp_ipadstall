#!/usr/bin/env bash
set -eu

print_usage() {
    printf "build.sh -e <staging|production> -v <version-string>"
}

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
ENVIRONMENT_FILE_PATH=$(realpath "$SCRIPT_DIR/../.projectconfig")

# Load our environment variables.
# shellcheck source=../.projectconfig
source "$ENVIRONMENT_FILE_PATH"

DOCKER_REGISTRY_HOSTNAME="docker-hub.tankbar.com"
DOCKER_IMAGE="$DOCKER_REGISTRY_HOSTNAME/$PROJECT_DOMAIN"

ENVIRONMENT="staging"
DRY_RUN=false
while getopts 'e:k:p:n' flag; do
    case "${flag}" in
        e) ENVIRONMENT="${OPTARG}" ;;
        n) DRY_RUN=true ;;
        *)
            print_usage
            exit 1
            ;;
    esac
done

if [ -z "$ENVIRONMENT" ]; then
    echo "Missing environment."
    print_usage

    exit 1
fi

if [ "$ENVIRONMENT" = "staging" ]; then
    VERSION="beta"
fi

docker build -t "$DOCKER_IMAGE:$VERSION" .

if [ $DRY_RUN = false ]; then
    docker image push "$DOCKER_IMAGE:$VERSION"
fi

if [ "$ENVIRONMENT" = "production" ]; then
    docker image tag "$DOCKER_IMAGE:$VERSION" "$DOCKER_IMAGE:latest"
    if [ $DRY_RUN = false ]; then
        docker image push "$DOCKER_IMAGE:latest"
    fi
fi
