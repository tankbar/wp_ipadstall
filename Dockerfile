# Compile the themes and plugins into a WordPress site.
FROM wordpress:6.1.1-php7.4-apache

WORKDIR /usr/src/wordpress

RUN set -eux; \
    mkdir -p /var/cache/pagespeed && \
    chown -R 33:33 /var/cache/pagespeed && \
    curl -O https://dl-ssl.google.com/dl/linux/direct/mod-pagespeed-stable_current_amd64.deb && \
    dpkg -i mod-pagespeed-*.deb && \
    rm mod-pagespeed-*.deb && \
    a2enmod headers && \
    pecl install -o -f redis && \
    rm -rf /tmp/pear && \
    docker-php-ext-enable redis && \
    find /etc/apache2 -name '*.conf' -type f -exec sed -ri -e "s!/var/www/html!$PWD!g" -e "s!Directory /var/www/!Directory $PWD!g" '{}' +; \
    cp -s wp-config-docker.php wp-config.php && \
    curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
    chown 33:33 wp-cli.phar && chmod +x wp-cli.phar && \
    mv wp-cli.phar /usr/local/bin/wp && \
    rm -r ./wp-content/themes/twenty* && \
    wp core download --version=6.6.1 --force --allow-root

COPY ./.config/apache/pagespeed.conf /etc/apache2/mods-enabled/pagespeed.conf
COPY ./.config/php/php.ini /usr/local/etc/php/conf.d/tankbar-php.ini
COPY --chown=33:33 ./.config/apache/.htaccess ./.htaccess
COPY --chown=33:33 ./themes ./wp-content/themes
COPY --chown=33:33 ./plugins ./wp-content/plugins
COPY --chown=33:33 ./languages ./wp-content/languages
COPY --chown=33:33 ./mu-plugins ./wp-content/mu-plugins

USER root:root
